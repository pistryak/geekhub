package com.pistryak.ast;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.PrefixExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.syntax.Token;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;

import java.lang.reflect.Modifier;

@GroovyASTTransformation
public class AutoGenerateIdASTTransformation implements ASTTransformation {
    private static final String COUNTER_FIELD_NAME = "COUNTER";
    private static final String ID_FIELD_NAME = "id";

    @Override
    public void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        ClassNode classNode = (ClassNode) astNodes[1];
        classNode.addField(createCounterField(classNode));
        classNode.addField(createIdField(classNode));
    }

    private FieldNode createCounterField(ClassNode owner) {
        return new FieldNode(
                COUNTER_FIELD_NAME,
                Modifier.PRIVATE | Modifier.STATIC,
                new ClassNode(Integer.class),
                owner,
                new ConstantExpression(0)
        );
    }

    private FieldNode createIdField(ClassNode owner) {
        return new FieldNode(
                ID_FIELD_NAME,
                Modifier.PUBLIC | Modifier.FINAL,
                new ClassNode(Integer.class),
                owner,
                createCounterIncrementExpression()
        );
    }

    private PrefixExpression createCounterIncrementExpression() {
        return new PrefixExpression(
                Token.newSymbol("++", -1, -1),
                new VariableExpression(COUNTER_FIELD_NAME, new ClassNode(Integer.class))
        );
    }
}
