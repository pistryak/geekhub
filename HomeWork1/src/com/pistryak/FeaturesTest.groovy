package com.pistryak

def name = 'Groovy'
def greeting = "Hello, ${name}"
assert "Hello, Groovy" == greeting

def sizes = ['Java', 'Groovy', 'Scala']*.size()
assert [4, 6, 5] == sizes

def a = null
def result = a ?: "hello"
assert "hello" == result