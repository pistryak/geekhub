package com.pistriak

import com.pistryak.University
import grails.test.spock.IntegrationSpec

class UniversityIntegrationSpec extends IntegrationSpec {
    void "test save"() {
        given:
        University university = new University(name: "CDTU")

        when:
        university.save()

        then:
        null != university.id
    }
}
