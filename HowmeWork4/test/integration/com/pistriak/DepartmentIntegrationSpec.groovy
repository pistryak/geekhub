package com.pistriak

import com.pistryak.Department
import com.pistryak.University
import grails.test.spock.IntegrationSpec

class DepartmentIntegrationSpec extends IntegrationSpec {
    void "test save"() {
        given:
        University university = new University(name: "CDTU")
        Department department = new Department(name: "PZAS", university: university)

        when:
        university.save()
        department.save()

        then:
        null != department.id
        null != university.id
    }
}
