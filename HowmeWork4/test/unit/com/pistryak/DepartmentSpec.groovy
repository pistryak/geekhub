package com.pistryak

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Department)
class DepartmentSpec extends Specification {
    void "test constrains"() {
        given:
        Department department = new Department(name: "PZAS")

        when:
        department.validate()

        then:
        0 == department.errors.errorCount
    }

    void "test constrains with with blank name"() {
        given:
        Department department = new Department()

        when:
        department.validate()

        then:
        1 == department.errors.errorCount
    }
}
