package com.pistryak

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(University)
class UniversitySpec extends Specification {
    void "test constrains"() {
        given:
        University university = new University(name: "CDTU")

        when:
        university.validate()

        then:
        0 == university.errors.errorCount
    }

    void "test constrains with with blank name"() {
        given:
        University university = new University()

        when:
        university.validate()

        then:
        1 == university.errors.errorCount
    }
}