package com.pistryak

class University {
    static hasMany = [departments: Department]

    String name

    static constraints = {
        name blank: false
    }
}