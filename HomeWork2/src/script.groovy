import com.pistryak.FirstFileProcessor
import com.pistryak.SecondFileProcessor
import com.pistryak.ThirdFileProcessor

import static com.pistryak.FileUtils.*

def pathToDir = args[0]
if (pathToDir == null) {
    return
}

regexpsAndFileProcessors = [
        /^a_.*$/: new FirstFileProcessor(),
        /^d_.*$/: new SecondFileProcessor("yyyy-MM-dd HH-mm-ss"),
        /^1_.*$/: new ThirdFileProcessor()
]

def processedFilePrefix = "done_"

regexpsAndFileProcessors.each {
    regexp, fileProcessor ->
        def files = getByRegexp(pathToDir, regexp)
        files = fileProcessor.processAll(files)
        files.each { addPrefix(it, processedFilePrefix) }
}