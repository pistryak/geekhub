package com.pistryak

import java.text.SimpleDateFormat

class SecondFileProcessor extends FileProcessor {
    String dateFormatPattern

    SecondFileProcessor(String dateFormatPattern) {
        this.dateFormatPattern = dateFormatPattern
    }

    @Override
    void process(File file) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatPattern)
        file << dateFormat.format(new Date())
        file
    }
}
