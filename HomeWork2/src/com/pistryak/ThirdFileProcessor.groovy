package com.pistryak

class ThirdFileProcessor extends FileProcessor {
    RandomMatrix matrix

    ThirdFileProcessor() {
        matrix = new RandomMatrix()
    }

    @Override
    void process(File file) {
        file << matrix
        file << matrix.sumOfMainDiagonal
        file << matrix.sumOfAntiDiagonal
    }
}
