package com.pistryak

class FirstFileProcessor extends FileProcessor {
    @Override
    void process(File file) {
        file << calculateCharsA(file.getName())
        file
    }

    private int calculateCharsA(String str) {
        int count = 0
        str.toCharArray().each { char ch ->
            if (ch == 'a') {
                count++;
            }
        }
        count
    }
}