package com.pistryak

class RandomMatrix {
    private static int SIZE = 4

    int[][] matrix

    RandomMatrix() {
        generateMatrix()
    }

    int getSumOfMainDiagonal() {
        int sum = 0
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (i == j) {
                    sum += matrix[i][j]
                }
            }
        }
        sum
    }

    int getSumOfAntiDiagonal() {
        int sum = 0
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (i == SIZE - i + 1) {
                    sum += matrix[i][j]
                }
            }
        }
        sum
    }

    private void generateMatrix() {
        Random rnd = new Random(10)
        matrix = new int[SIZE][SIZE]
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = rnd.nextInt();
            }
        }
    }


    @Override
    String toString() {
        "RandomMatrix{" +
                "matrix=" + Arrays.toString(matrix) +
                '}';
    }
}
