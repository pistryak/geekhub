package com.pistryak

class FileUtils {
    static File addPrefix(File file, String prefix) {
        def newFileName = "${file.getParent()}/${prefix}${file.getName()}"
        file.renameTo(newFileName)
        new File(newFileName)
    }

    static File[] getByRegexp(String pathToDir, String regexp) {
        new File(pathToDir).listFiles({d, f-> f ==~ regexp } as FilenameFilter).toList()
    }
}