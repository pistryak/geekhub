package com.pistryak

abstract class FileProcessor {
    abstract void process(File file);

    void processAll(File[] files) {
        files.each { process(it) }
    }
}