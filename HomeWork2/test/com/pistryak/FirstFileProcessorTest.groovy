package com.pistryak

import org.junit.rules.TemporaryFolder

class FirstFileProcessorTest extends GroovyTestCase {
    FirstFileProcessor fileProcessor;
    TemporaryFolder tmpFolder

    @Override
    void setUp() {
        fileProcessor = new FirstFileProcessor()
        tmpFolder = new TemporaryFolder()
        tmpFolder.create()
    }

    void testProcess() {
        File file = tmpFolder.newFile("aaaaa");
        fileProcessor.process(file)
        assertEquals("5", file.text)
    }

    @Override
    void tearDown() {
        tmpFolder.delete()
    }
}
