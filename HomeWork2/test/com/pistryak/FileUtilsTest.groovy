package com.pistryak

import org.junit.rules.TemporaryFolder

class FileUtilsTest extends GroovyTestCase {
    TemporaryFolder tmpFolder

    @Override
    void setUp() {
        tmpFolder = new TemporaryFolder()
        tmpFolder.create()
    }

    void testAddPrefix() {
        File file = tmpFolder.newFile("example")
        File fileWithPrefix = FileUtils.addPrefix(file, "done_")
        assertFalse(file.isFile());
        assertTrue(fileWithPrefix.isFile())
    }

    void testGetByRegexp() {
        def file1 = tmpFolder.newFile("a_groovy")
        def file2 = tmpFolder.newFile("a_java")
        def file3 = tmpFolder.newFile("php")
        def file4 = tmpFolder.newFile("hello")

        def actual = FileUtils.getByRegexp(tmpFolder.root.getPath(), /^a_.*$/)
        assertTrue(actual.contains(file1))
        assertTrue(actual.contains(file2))
        assertFalse(actual.contains(file3))
        assertFalse(actual.contains(file4))
    }

    @Override
    void tearDown() {
        tmpFolder.delete()
    }
}
