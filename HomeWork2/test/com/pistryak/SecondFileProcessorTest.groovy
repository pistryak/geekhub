package com.pistryak

import org.junit.rules.TemporaryFolder

import java.text.SimpleDateFormat

class SecondFileProcessorTest extends GroovyTestCase {
    SecondFileProcessor fileProcessor
    TemporaryFolder tmpFolder

    @Override
    void setUp() {
        fileProcessor = new SecondFileProcessor("yyyy-MM-dd HH-mm-ss")
        tmpFolder = new TemporaryFolder()
        tmpFolder.create()
    }

    void testProcess() {
        File file = tmpFolder.newFile("test")
        fileProcessor.process(file)
        String date = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date())
        assertEquals(date, file.text)
    }

    @Override
    void tearDown() {
        tmpFolder.delete()
    }
}
